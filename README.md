# moz-playwithmpv

Simple legacy Mozilla add-on which allows adding a toolbar button to open media
(audio and videos) and view videos on popular sites online with the mpv media
player.
On SeaMonkey you can also add the button to the "Mails & Newsgroup" component,
so you can open with mpv items on your Feeds :)

It is similar to the [play-with-mpv web extension](https://github.com/Thann/play-with-mpv),
but it does not require running a server to handle play requests (something
impossible to achieve with modern web extensions...)

## Requirements
- SeaMonkey or Firefox <= 56
- mpv
- youtube-dl (to view videos from YouTube and other popular video websites)

## Installation
Just compress everything to a .zip file, optionally rename it to have an xpi
extension, and install it normally via the Add-ons manager.

## Usage
- Pressing the button launches mpv with the website passed as a parameter.
- If it cannot open it, an error message will show.
- While the media is being played, toggling the button will close mpv.
