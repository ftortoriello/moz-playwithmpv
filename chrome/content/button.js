'use strict';

/* TODO: Use only one global var */

var playWithMPV_Process = null;
var playWithMPV_Button = null;

function PlayWithMPV_ObserveProcessExit(subject, topic, data) {
	if (playWithMPV_Button.checked)  {
		playWithMPV_Button.checked = false;

		var stringsBundle = document.getElementById('button-strings');
		if (topic != 'process-finished') {
			alert(stringsBundle.getString('exitError') + '. ' + stringsBundle.getString('exitCode') + ': ' + subject.exitValue);
		} else if (!isNaN(subject.exitValue) && (0 != subject.exitValue)) {
			alert(stringsBundle.getString('exitFail') + '. ' + stringsBundle.getString('exitCode') + ': ' + subject.exitValue);
		}
	}
}

function PlayWithMPV_RunMPV(url) {
	function LoadExe() {
		const MPVPATH = '/usr/bin/mpv';

		var file = Components.classes['@mozilla.org/file/local;1']
			.createInstance(Components.interfaces.nsIFile);
		file.initWithPath(MPVPATH);

		var stringsBundle = document.getElementById('button-strings');

		if (!file.exists()) {
			alert(stringsBundle.getString('inexistentFile') + ': ' + MPVPATH);
			return null;
		}

		if (!file.isExecutable()) {
			alert(stringsBundle.getString('fileNoExe') + ': ' + MPVPATH);
			return null;
		}

		return file;
	}

	if (url == '') return false;

	var exe = LoadExe();
	if (!exe) return false;

	playWithMPV_Process = Components.classes['@mozilla.org/process/util;1']
		.createInstance(Components.interfaces.nsIProcess);
	playWithMPV_Process.init(exe);

	var args = [ url ];
	playWithMPV_Process.runAsync(args, args.length, PlayWithMPV_ObserveProcessExit);
	return true;
}

function PlayWithMPV_KillMPV() {
	if (playWithMPV_Process) {
		playWithMPV_Process.kill();
		playWithMPV_Process = null;
	}
}

function PlayWithMPV() {
	if (!playWithMPV_Button) {
		playWithMPV_Button = document.getElementById('playwithmpv-button');
	}

	if (playWithMPV_Button.checked) {
		/* Get URL string of the current tab */
		var windowMediator = Components.classes['@mozilla.org/appshell/window-mediator;1']
			.getService(Components.interfaces.nsIWindowMediator);
		/* document frame inside the chrome */
		var browser = windowMediator.getMostRecentWindow('navigator:browser').getBrowser();
		if (!browser) return;
		var url = browser.currentURI.spec;

		/* Pause all videos */
		for (const video of content.document.getElementsByTagName('video')) {
			video.pause();
		}
		
		if (!PlayWithMPV_RunMPV(url)) {
			playWithMPV_Button.checked = false;
		}
	} else {
		/* !playWithMPV_Button.checked -> Kill process */
		PlayWithMPV_KillMPV();
	}
}

function PlayWithMPV_Mail() {
	if (!playWithMPV_Button) {
		playWithMPV_Button = document.getElementById('playwithmpv-mail-button');
	}

	if (playWithMPV_Button.checked) {
		/* Get feed URL */
		var url = document.getAnonymousElementByAttribute(
			document.getElementById('expandedcontent-baseBox'), 'anonid', 'headerValue')
			.value;

		if (!PlayWithMPV_RunMPV(url)) {
			playWithMPV_Button.checked = false;
		}
	} else {
		/* !playWithMPV_Button.checked -> Kill process */
		PlayWithMPV_KillMPV();
	}
}
